using System;
using Xunit;

using CalculatorTest.Core;

namespace CalculatorTest.Tests
{
    public class SimpleCalculatorDivideTest
    {
        [Theory]
        [InlineData(0, 1, 0)]
        [InlineData(0, -1, 0)]
        [InlineData(2, 1, 2)]
        [InlineData(3, 1, 3)]
        [InlineData(5, 2, 2)]
        public void CanDivideTheory(int value1, int value2, int expected)
        {
            var fixture = Fixture.Sut();
            var calculator = fixture.Item1;
            var diagnosticMock = fixture.Item2;

            var result = calculator.Divide(value1, value2);
            Assert.Equal(expected, result);

            diagnosticMock.Verify(x => x.Log(value1, value2, Operation.Divide), "Expected diagnostics call");
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        [InlineData(-1)]
        public void DivideByZero_ThrowsDivideByZeroException(int value1)
        {
            var fixture = Fixture.Sut();
            var calculator = fixture.Item1;
            var diagnosticMock = fixture.Item2;

            Assert.Throws<DivideByZeroException>(() => calculator.Divide(value1, 0));

            diagnosticMock.Verify(x => x.Log(value1, 0, Operation.Divide), "Expected diagnostics call");

        }
    }
}
