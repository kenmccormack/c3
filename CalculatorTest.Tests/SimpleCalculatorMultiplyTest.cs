using System;
using Xunit;

using CalculatorTest.Core;

namespace CalculatorTest.Tests
{
    public class SimpleCalculatorMultiplyTest
    {
        [Theory]
        [InlineData(0, 0, 0)]
        [InlineData(0, 1, 0)]
        [InlineData(0, -1, 0)]
        [InlineData(1, 1, 1)]
        [InlineData(1, -1, -1)]
        [InlineData(1, 2, 2)]
        public void CanMultiplyTheory(int value1, int value2, int expected)
        {
            var fixture = Fixture.Sut();
            var calculator = fixture.Item1;
            var diagnosticMock = fixture.Item2;

            var result = calculator.Multiply(value1, value2);
            Assert.Equal(expected, result);

            diagnosticMock.Verify(x => x.Log(value1, value2, Operation.Multiply), "Expected diagnostics call");
        }
    }
}
