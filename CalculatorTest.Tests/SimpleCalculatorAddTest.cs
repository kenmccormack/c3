using System;
using Xunit;

using CalculatorTest.Core;

namespace CalculatorTest.Tests
{
    public class SimpleCalculatorAddTest
    {
        [Theory]
        [InlineData(0, 0, 0)]
        [InlineData(0, 1, 1)]
        [InlineData(1, 1, 2)]
        [InlineData(-1, 1, 0)]
        [InlineData(1, -1, 0)]
        [InlineData(1, 2, 3)]
        //if checked removed [InlineData(int.MinValue, -1, int.MaxValue)]
        //if checked removed [InlineData(int.MaxValue, 1, int.MinValue)]
        public void CanAddTheory(int value1, int value2, int expected)
        {
            var fixture = Fixture.Sut();
            var calculator = fixture.Item1;
            var diagnosticMock = fixture.Item2;

            var result = calculator.Add(value1, value2);
            Assert.Equal(expected, result);

            diagnosticMock.Verify(x => x.Log(value1, value2, Operation.Add), "Expected diagnostics call");
        }


        [Fact]
        public void Add_NumericOverflow_ThrowsException()
        {
            var fixture = Fixture.Sut();
            var calculator = fixture.Item1;
            var diagnosticMock = fixture.Item2;

            //This assert pins in 'checked' (default behaviour will overflow to 2's complement)
            Assert.Throws<OverflowException>(() => calculator.Add(int.MaxValue, 100));
            diagnosticMock.Verify(x => x.Log(int.MaxValue, 100, Operation.Add), "Expected diagnostics call");
        }
    }
}
