using System;
using Xunit;

using CalculatorTest.Core;

namespace CalculatorTest.Tests
{
    public class SimpleCalculatorSubtractTest
    {
        [Theory]
        [InlineData(0, 0, 0)]
        [InlineData(0, 1, -1)]
        [InlineData(0, -1, 1)]
        [InlineData(1, 1, 0)]
        [InlineData(1, -1, 2)]
        [InlineData(1, 2, -1)]
        [InlineData(int.MinValue, 1, int.MaxValue)]
        [InlineData(int.MaxValue, -1, int.MinValue)]
        public void CanSubtractTheory(int value1, int value2, int expected)
        {
            var fixture = Fixture.Sut();
            var calculator = fixture.Item1;
            var diagnosticMock = fixture.Item2;

            var result = calculator.Subtract(value1, value2);
            Assert.Equal(expected, result);

            diagnosticMock.Verify(x => x.Log(value1, value2, Operation.Subtract), "Expected diagnostics call");
        }
    }
}
