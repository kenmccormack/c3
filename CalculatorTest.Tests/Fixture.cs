using System;
using Xunit;
using Moq;
using CalculatorTest.Core;
using CalculatorTest.Core.Diagnostics;

namespace CalculatorTest.Tests
{
    internal static class Fixture
    {
        public static Tuple<ISimpleCalculator, Mock<ICalculatorDiagnostic>> Sut()
        {
            var diagnostics = new Mock<ICalculatorDiagnostic>();
            var calculator = new SimpleCalculator(diagnostics.Object) as ISimpleCalculator;
            return new Tuple<ISimpleCalculator, Mock<ICalculatorDiagnostic>>(calculator, diagnostics);
        }
    }
}
