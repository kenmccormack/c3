﻿using System;
using Microsoft.AspNetCore.Mvc;

using CalculatorTest.Core;
using CalculatorTest.Core.Diagnostics;

namespace CalculatorTest.API.Controllers
{
    [Route("api/[controller]")]
    public class CalculatorController : Controller
    {
        // GET api/calculator/Add/5/6
        [HttpGet("{operation}/{param1}/{param2}")]
        public int Get(Operation operation, int param1, int param2)
        {
            var calculator = new CalculatorAdaptor(new SimpleCalculator(new NoLogDiagnostic()));
            return calculator.DoOperation(operation, param1, param2);
        }
    }
}
