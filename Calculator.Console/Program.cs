﻿namespace Calculator.Console
{
    class Program
    {
        private static void Main(string[] args)
        {
            // calculator: specify args -api | (local is the default)
            // diagnostics: specify args -sql | -nolog (console is the default)
            var calculator = CalculatorFactory.GetCalculator(new Config(), args); 
            var console = new ConcreteConsole();

            // program dependencies abstracted (esp. System.Console)
            // the program could now be easily acceptance tested
            var program = new CalculatorProgram(console, calculator);

            program.Run();
        }
    }
}
