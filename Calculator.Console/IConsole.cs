﻿namespace Calculator.Console
{
    public interface IConsole
    {
        string ReadLine();
        void WriteLine(string message);
    }
}
