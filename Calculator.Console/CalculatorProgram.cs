﻿using System;

using CalculatorTest.Core;

namespace Calculator.Console
{
    public sealed class CalculatorProgram
    {
        private readonly IConsole console;
        private readonly ICalculatorAdaptor calculator;

        public CalculatorProgram(IConsole console, ICalculatorAdaptor calculator)
        {
            this.console = console ?? throw new ArgumentNullException(nameof(console));
            this.calculator = calculator ?? throw new ArgumentNullException(nameof(calculator)); ;    
        }

        public void Run()
        {
            console.WriteLine("Calculator Console!");

            while(true) 
            {
                console.WriteLine("(Type exit at any time to quit)");

                var input1 = GetIntegerInputOrNullExit("Start (an integer)");
                if (!input1.HasValue) break;

                var input2 = GetIntegerInputOrNullExit("By (an integer)");
                if (!input2.HasValue) break;

                var op = GetOperation();
                if (!op.HasValue) break;

                var result = calculator.DoOperation(op.Value, input1.Value, input2.Value);
                console.WriteLine(result.ToString());
            }

            console.WriteLine("Exiting Calculator Console");
        }

        private int? GetIntegerInputOrNullExit(string argumentName)
        {
            while (true)
            {
                // TODO: abstract Console and unit test to acceptance level
                console.WriteLine("Enter " + argumentName + ":");

                var input = console.ReadLine();
                if (input == "exit") return null;

                int result;
                if (int.TryParse(input, out result)) return result;

                console.WriteLine("Invalid input! Pls enter a valid integer or type 'exit'");
            }
        }

        private Operation? GetOperation()
        {
            while (true)
            {
                console.WriteLine("Enter operation add(+), subtract(-), multuply(*), divide(-), or 'exit':");

                var input = console.ReadLine();
                if (input == "exit") return null;

                switch(input)
                {
                    case "exit":
                        return null;
                    case "+":
                        return Operation.Add;
                    case "-":
                        return Operation.Subtract;
                    case "/":
                        return Operation.Divide;
                    case "*":
                        return Operation.Multiply;
                }

                console.WriteLine("Unrecognised input!");
            }
        }
    }
}
