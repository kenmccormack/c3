using System;
using System.Linq;
using CalculatorTest.Core;
using CalculatorTest.Core.Diagnostics;

namespace Calculator.Console
{
    public static class CalculatorFactory
    {
        public static ICalculatorAdaptor GetCalculator(Config config, string[] args)
        {
            var diagnostic = GetDiagnostic(config, args);

            if (args.Any(a => a == "-api"))
            {
                return new CalculatorApiAdaptor(diagnostic, config.BaseApiUrl);
            }

            return new CalculatorAdaptor(new SimpleCalculator(diagnostic));
        }

        private static ICalculatorDiagnostic GetDiagnostic(Config config, string[] args)
        {
            if (args.Any(a => a == "-sql"))
            {
                return new MsSqlDiagnostic(config.CubicConnectionString);
            }

            if (args.Any(a => a == "-nolog"))
            {
                return new NoLogDiagnostic();
            }

            return new ConsoleDiagnostic();
        }
    }
}
