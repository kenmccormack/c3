using System;

namespace Calculator.Console
{
    public sealed class Config
    {
        // TODO - move to config file and encrypt
        public string CubicConnectionString 
        {
            get { return "Server=127.0.0.1,1433;Database=Cubic;User Id=sa;Password=<YourStrong!Passw0rd>"; }
        }

        public string BaseApiUrl
        {
            get { return "http://localhost:17692/api/"; }
        }
    }
}