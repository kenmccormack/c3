﻿using System;
using System.Net.Http;
using CalculatorTest.Core;
using CalculatorTest.Core.Diagnostics;

namespace Calculator.Console
{
    public class CalculatorApiAdaptor : ICalculatorAdaptor
    {
        private static readonly HttpClient client = new HttpClient();
        private readonly ICalculatorDiagnostic diagnostic;
        private readonly string baseUrl;

        public CalculatorApiAdaptor(ICalculatorDiagnostic diagnostic, string baseUrl)
        {
            this.diagnostic = diagnostic;
            this.baseUrl = baseUrl ?? throw new ArgumentNullException(nameof(baseUrl));

            if (string.IsNullOrEmpty(baseUrl))
                throw new ArgumentOutOfRangeException("baseUrl cannot be empty");

            if (!baseUrl.EndsWith("/", StringComparison.OrdinalIgnoreCase)) 
            {
                this.baseUrl += "/";
            }
        }

        public int DoOperation(Operation operation, int input1, int input2)
        {
            return GetResult(operation, input1, input2);
        }

        private int GetResult(Operation operation, int input1, int input2)
        {
            diagnostic.Log(input1, input2, operation);

            var url = baseUrl + $"calculator/{operation}/{input1}/{input2}";
            var stringResult = client.GetStringAsync(url).Result;

            int result;
            if (int.TryParse(stringResult, out result))
            {
                return result;
            }

            throw new InvalidProgramException($"Invalid result: {result}");
        }
    }
}
