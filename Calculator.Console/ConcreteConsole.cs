namespace Calculator.Console
{
    public sealed class ConcreteConsole : IConsole
    {
        public string ReadLine()
        {
            return System.Console.ReadLine();
        }
        public void WriteLine(string message)
        {
            System.Console.WriteLine(message);
        }
    }
}
