create database Cubic
GO

Use Cubic
GO

CREATE TABLE CalculateLog (Id int IDENTITY(1,1) PRIMARY KEY, Param1 int NOT NULL, Param2 int NOT NULL, Operation int NOT NULL);
GO


CREATE PROCEDURE CalculateLogInsert
(@Param1 int, @Param2 int, @Operation int)

AS
INSERT INTO CalculateLog(Param1, Param2, Operation)
VALUES (@Param1, @Param2, @Operation)
GO

USE Cubic;
SELECT * from CalculateLog 