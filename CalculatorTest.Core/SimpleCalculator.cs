﻿using System;
using CalculatorTest.Core.Diagnostics;

namespace CalculatorTest.Core
{
    public class SimpleCalculator : ISimpleCalculator
    {
        private readonly ICalculatorDiagnostic diagnostic;
        public SimpleCalculator(ICalculatorDiagnostic diagnostic)
        {
            this.diagnostic = diagnostic ?? throw new ArgumentNullException(nameof(diagnostic));
        }

        public int Add(int start, int amount)
        {
            diagnostic.Log(start, amount, Operation.Add);
            checked 
            {
                return start + amount;
            }
        }

        public int Divide(int start, int by)
        {
            diagnostic.Log(start, by, Operation.Divide);
            return start / by;
        }

        public int Multiply(int start, int by)
        {
            diagnostic.Log(start, by, Operation.Multiply);
            return start * by;
        }

        public int Subtract(int start, int amount)
        {
            diagnostic.Log(start, amount, Operation.Subtract);
            return start - amount;
        }
    }
}
