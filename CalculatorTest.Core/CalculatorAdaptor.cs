﻿using System;
namespace CalculatorTest.Core
{
    public class CalculatorAdaptor : ICalculatorAdaptor
    {
        private readonly ISimpleCalculator calculator;

        public CalculatorAdaptor(ISimpleCalculator calculator)
        {
            this.calculator = calculator ?? throw new ArgumentNullException(nameof(calculator));
        }

        public int DoOperation(Operation operation, int input1, int input2)
        {
            switch (operation)
            {
                case Operation.Add:
                    return calculator.Add(input1, input2);

                case Operation.Subtract:
                    return calculator.Subtract(input1, input2);

                case Operation.Multiply:
                    return calculator.Multiply(input1, input2);

                case Operation.Divide:
                    return calculator.Divide(input1, input2);

                default:
                    throw new NotImplementedException($"Operation type {operation} not supported.");
            }
        }

    }
}
