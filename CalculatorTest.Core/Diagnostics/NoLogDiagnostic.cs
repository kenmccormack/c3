using CalculatorTest.Core;

namespace CalculatorTest.Core.Diagnostics
{
    public class NoLogDiagnostic : ICalculatorDiagnostic
    {
        public void Log(int arg1, int arg2, Operation operation)
        {
            // does nothing
        }
    }
}
