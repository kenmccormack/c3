﻿using System;
using CalculatorTest.Core;

namespace CalculatorTest.Core.Diagnostics
{
    public class ConsoleDiagnostic : ICalculatorDiagnostic
    {
        public void Log(int arg1, int arg2, Operation operation)
        {
            System.Console.WriteLine($"arg1: {arg1}, arg2: {arg2}, op: {operation}");
        }
    }
}
