
namespace CalculatorTest.Core.Diagnostics
{
    public interface ICalculatorDiagnostic
    {
        void Log(int arg1, int arg2, Operation operation);
    }
}
