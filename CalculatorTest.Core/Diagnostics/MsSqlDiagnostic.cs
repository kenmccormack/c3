﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace CalculatorTest.Core.Diagnostics
{
    public class MsSqlDiagnostic : ICalculatorDiagnostic
    {
        private readonly string connectionString;

        public MsSqlDiagnostic(string connectionString)
        {
            this.connectionString = connectionString ?? throw new ArgumentNullException(nameof(connectionString));
        }

        public void Log(int arg1, int arg2, Operation operation)
        {
            StoreDiagnostic(arg1, arg2, (int)operation);
        }

        private async void StoreDiagnostic(int arg1, int arg2, int operation)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (var cmd = new SqlCommand("CalculateLogInsert", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Param1", arg1);
                    cmd.Parameters.AddWithValue("@Param2", arg2);
                    cmd.Parameters.AddWithValue("@Operation", operation);
                    await cmd.ExecuteNonQueryAsync();
                }
            }
        }
    }
}
