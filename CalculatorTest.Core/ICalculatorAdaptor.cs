﻿namespace CalculatorTest.Core
{
    public interface ICalculatorAdaptor
    {
        int DoOperation(Operation operation, int input1, int input2);
    }
}
